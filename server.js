require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose')
const app = express();
const port = process.env.PORT || 8000;

// Dependencies middleware
//const bodyParser = require('body-parser');
//const compression = require('compression');
//const cors = require('cors');
//const helmet = require('helmet');

// Core
//const config = require('./config');
//const routes = require('./controllers/routes');

// connection a la base de données
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("MongoDB Connected..."))
  .catch((err) => console.log(err));


// creation de la session
// app.use(
//     session({
//         secret: process.env.SESSION_SECRET,
//         resave: false,
//         saveUninitialized: false,
//         cookie: { secure: false },
//     })
// );

app.get('/', (req, res) => {
    res.set('Content-Type', 'text/html');
    res.send('Hello world !!');
});

app.listen(port, () => {
    console.log('Server app listening on port ' + port);
});